# cah

![](https://img.shields.io/badge/written%20in-Javascript%20%28node.js%29-blue)

Game server, similar to Cards Against Humanity.

## See Also

To install: `npm install cah`
See `node cah/server.js -h` for options.

https://www.npmjs.org/package/cah


## Download

- [⬇️ cah-1.0.0.tgz](dist-archive/cah-1.0.0.tgz) *(48.40 KiB)*
